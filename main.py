from models import Books, connection

class database:
    def showBooks(self):
        try:
            for doc in Books.objects:
                print (doc.pk, doc.nama, doc.pengarang, doc.tahunterbit,doc.genre)
        except Exception as e:
            print(e)
        pass
    def showBookById(self, **params):
        try:
            for doc in Books.objects(pk=params['id']):
                print (doc.pk, doc.nama, doc.pengarang, doc.tahunterbit,doc.genre)
        except Exception as e:
            print(e)
        pass
    def searchBookByName(self, **params):
        try:
            for doc in Books.objects(nama=params['nama']):
                print (doc.pk, doc.nama, doc.pengarang, doc.tahunterbit,doc.genre)
        except Exception as e:
            print(e)
        pass
    def insertBook(self,**params):
        try:
            Books(**params).save()
            print(Books.objects().pk)
        except Exception as e:
            print(e)
        pass
    def updateBookById(self,**params):
        try:
            docs = Books.objects(pk=params['id']).first()
            print (docs.to_json())

            docs.tahunterbit = params['tahunterbit']
            docs.save()
            print("After Update")
            docs = Books.objects(pk=params['id']).first()
            print (docs.to_json())
        except Exception as e:
            print(e)
        pass
    def deleteBookById(self,**params):
        try:
            docs = Books.objects(pk=params['id']).first()
            print (docs.to_json())
            docs.delete()
        except Exception as e:
            print(e)
        pass


if __name__ == "__main__":
    db = database()
    db.showBooks()

    params = {"id":"6100de52d4063db349026855"}
    db.showBookById(**params)

    params = {"nama":"A Higher Loyalty: Truth, Lies, and Leadership"}
    db.searchBookByName(**params)

    params = {
        "nama" : "Belajar Bikin Buku",
        "pengarang" : "Taufik Adi Saputra",
        "tahunterbit" : "2021",
        "genre" : "Random"
    }
    db.insertBook(**params)

    params = {
        "id":"6100de52d4063db349026855",
        "tahunterbit" : "2017",
    }
    db.updateBookById(**params)

    params = {"id":"6100de52d4063db349026855"}
    db.deleteBookById(**params)