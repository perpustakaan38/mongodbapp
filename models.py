from sqlalchemy.log import Identified
from mongoengine import connect
from mongoengine import Document, StringField, DynamicDocument

connection = connect(db="perpustakaan", host="localhost", port=27017)

if connection:
    print("MongoDB Connected")
    
class Books(Document):
    nama = StringField(required=True, max_length=100)
    pengarang = StringField(required=True, max_length=100)
    tahunterbit = StringField(required=True, max_length=20)
    genre = StringField(required=True, max_length=20)